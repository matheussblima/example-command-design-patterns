import services, {PeopleResponse} from 'services';
import {observable, makeObservable, runInAction, action} from 'mobx';
declare type PeopleAction = 'success' | 'failure';

export interface PeopleResult {
  action: PeopleAction;
  error?: string;
}

export class PeopleModel {
  @observable
  public peoples: PeopleResponse[] | [] = [];

  constructor() {
    makeObservable(this);
  }

  @action
  public async getPeoples(): Promise<PeopleResult> {
    const response = await services.getPeoples();

    if (response.status === 200) {
      const data = response.data as PeopleResponse[];

      runInAction(() => (this.peoples = data));

      return {
        action: 'success',
      };
    }

    return {
      action: 'failure',
      error: 'Não foi possível buscar os personagens',
    };
  }
}

export const People = new PeopleModel();
