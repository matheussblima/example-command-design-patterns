import React, { useEffect } from 'react';
import { ActivityIndicator, FlatList } from 'react-native';
import { observer } from 'mobx-react';
import { Container, Label, ListItem } from 'components';
import { useCommand } from 'hooks';
import { PeoplesCommand, CommandInvoker } from 'commands';
import { People } from 'models';

interface IHomeProps {}

interface IRenderItem {
  name: string;
}

export const Home: React.FC<IHomeProps> = observer(() => {
  const peoplesCommand = useCommand(() => new PeoplesCommand());
  useEffect(() => CommandInvoker(peoplesCommand)(), [peoplesCommand]);

  return (
    <Container>
      <Label>Lista de personagens Star Wars</Label>
      {peoplesCommand.loading && <ActivityIndicator size="small" />}
      <FlatList<IRenderItem>
        data={People.peoples}
        renderItem={(result: { item: IRenderItem }) => (
          <ListItem label={result.item.name} />
        )}
        keyExtractor={(item, index) => `${item.name}_${index}`}
      />
    </Container>
  );
});
