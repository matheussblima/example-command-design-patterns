import {action, observable, makeObservable, runInAction} from 'mobx';
import {Command} from 'commands';
import {People} from 'models';

export interface IPeopleCommandParams {}

export class PeoplesCommand implements Command {
  @observable
  public loading: boolean = false;

  @observable
  public error: string | null = null;

  constructor() {
    makeObservable(this);
  }

  @action
  public async execute(): Promise<void> {
    try {
      this.loading = true;
      await People.getPeoples();
    } catch (err) {
      this.error = 'Error ao buscar os dados';
    } finally {
      runInAction(() => (this.loading = false));
    }
  }

  @action
  public canExecute(): boolean {
    return true;
  }
}
