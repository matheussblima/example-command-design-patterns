import {PeoplesCommand} from 'commands';
import {People} from 'models';

describe('Peoples Command', () => {
  it('execute get peoples', () => {
    const store = new PeoplesCommand();
    expect(store.loading).toBe(false);
    store.execute().then(() => {
      expect(store.loading).toBe(false);
      expect(People.peoples.length).toBeGreaterThan(0);
    });
    expect(store.loading).toBe(true);
    expect(store.error).toBe(null);
  });
});
