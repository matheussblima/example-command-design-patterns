export * from './service';
import {getPeoples, PeopleResponse} from './people.service';

export type {PeopleResponse};
export default {getPeoples};
