import {Response} from 'services';

export interface PeopleResponse {
  name: string;
}

export const getPeoples = async (): Promise<Response<PeopleResponse[]>> => {
  try {
    const response = await fetch('https://swapi.dev/api/people', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    });
    const result: Response<PeopleResponse[]> = {
      status: response.status,
    };

    if (response.ok) {
      const body = await response.json();
      result.data = body.results as PeopleResponse[];
    }

    return result;
  } catch (error) {
    return {
      status: 0,
    };
  }
};
