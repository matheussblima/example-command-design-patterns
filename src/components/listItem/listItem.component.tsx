import React from 'react';
import styled from 'styled-components/native';
import {Label} from 'components';

export interface IListItemProps {
  label: string;
  backgroundColor?: string;
}

const Wrapper = styled.View`
  background-color: ${(props: IListItemProps) => props.backgroundColor};
  padding: 8px;
  margin-vertical: 8px;
  border-radius: 4px;
  align-items: center;
  justify-content: center;
`;

export const ListItem: React.FC<IListItemProps> = props => {
  return (
    <Wrapper {...props}>
      <Label>{props.label}</Label>
    </Wrapper>
  );
};

ListItem.defaultProps = {
  backgroundColor: '#606060',
};
