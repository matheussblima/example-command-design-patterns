import React from 'react';
import styled from 'styled-components/native';

export interface IContainerProps {
  children: any;
}

const Wrapper = styled.SafeAreaView`
  background-color: #000;
  flex: 1;
  padding: 16px;
`;

export const Container: React.FC<IContainerProps> = ({children}) => {
  return <Wrapper>{children}</Wrapper>;
};
