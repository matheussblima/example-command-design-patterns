import React from 'react';
import styled from 'styled-components/native';

export interface ILabelProps {
  children: string;
  color?: string;
  fontSize?: number;
  textAlign?: 'center' | 'left' | 'right';
}

const Text = styled.Text`
  color: ${(props: ILabelProps) => props.color};
  font-size: ${(props: ILabelProps) => props.fontSize}px;
  text-align: ${(props: ILabelProps) => props.textAlign};
`;

export const Label: React.FC<ILabelProps> = props => {
  return <Text {...props}>{props.children}</Text>;
};

Label.defaultProps = {
  color: '#FFF',
  fontSize: 18,
  textAlign: 'center',
};
